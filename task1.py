# Оценка ффективности в худшем случае - O(N)

def difference(arr_a, arr_b):
    return list(set(arr_a) - set(arr_b))

a = [0, 1, 0, 0, 4, 5, 6, 7, 0, 8, -4, 0]
b = [0, 1, 0, 0, 4, 5, 6, 0, 7, -4, 0]

print(difference(a, b))