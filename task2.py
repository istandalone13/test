def zero_del(a):
    r, w = 0, 0
    while (r < (len(a)-1) and w< len(a)):

        r += 1
        if a[w] != 0:
            w += 1
        elif a[r] != 0:
            a[w] = a[r]
            a[r] = 0
            w += 1

    while(a[-1] == 0):
        a.pop()
        
    return a


arr = [0, 1, 0, 0 , 4, 5, 6, 7, 0, 8, -4, 0]

print(zero_del(arr))

